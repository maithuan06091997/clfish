jQuery(document).ready(function () {
  jQuery(".post-info video").mouseover(function () {
    jQuery(this).prop("muted", true);
    jQuery(this).trigger("play");
  });
  jQuery(".post-info video").mouseout(function () {
    jQuery(this).trigger("pause");
  });
  jQuery(".bg-nav-menu").click(function () {
    jQuery(".nav-menu").fadeOut();
  });
  jQuery(".menu-mobile").click(function () {
    jQuery(".nav-menu").fadeIn();
  });

  jQuery(".nav-menu .list-nav-menu li").click(function () {
    jQuery(this).find("ul").fadeToggle();
  });

  jQuery(window).scroll(function () {
    const sticky = jQuery(".fish-header");
    const scroll = jQuery(window).scrollTop();
    if (scroll > sticky.height()) sticky.addClass("fixed");
    else sticky.removeClass("fixed");
  });
  jQuery(".logo-slider").slick({
    dots: false,
    autoplay: true,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 1,
    infinite: false,
    prevArrow: `<p class="slick-prev"><img src="img/prev.png" alt="#"></p>`,
    nextArrow: `<p class="slick-next"><img src="img/next.png" alt="#"></p>`,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  });
  jQuery.get(
    "https://liveprice.fpts.com.vn/HSX/data.ashx?s=quote&l=ACL",
    function (data, status) {
      const dataRes = jQuery.parseJSON(data);
      jQuery(".price-stock").text(
        Intl.NumberFormat("en-VN").format(dataRes[0].Info[9][1] * 1000)
      );
      jQuery(".number-stock").text(
        Intl.NumberFormat("en-VN").format(dataRes[0].Info[13][1] * 1000)
      );
    }
  );
});
AOS.init({
  once: true,
});
